package br.com.caelum.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HolaMundo extends HttpServlet{
	
	/**
	 * 
	 */
	/*private static final long serialVersionUID = 1L;*/

	@Override
	protected void service(HttpServletRequest resquest, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		
		out.println("<html>");
		out.println("<head>");
	    out.println("<title>Primera Servlet</title>");
	    out.println("</head>");
	    out.println("<body>");
	    out.println("<h1>Hola mundo Servlet!</h1>");
	    out.println("</body>");
	    out.println("</html>");
	}

}
